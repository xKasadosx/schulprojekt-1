<html>
<head>
	<meta charset="UTF-8">
    <meta name="description" content="Website: PowerBad GmbH" />
    <meta name="author" content="Hertz System AG" />
	<a href="index.php"><img src="../images/powerbad_logo.bmp" border="0"></a>
</head>

<body text="#000000" bgcolor="#F0DEFF" link="#000080" alink="#FF0000" vlink="#FF0000">
	<p align="left">
	<a href="../de/hilfe.php"><img src="../images/german_flag.jpg" height="25" width="37,5"/></a>
	<a href="help.php"><img src="../images/english_flag.jpg" height="25" width="37,5"/></a>
	</p>
	<hr/>
	<h1>Help</h1>
	<hr />
	<br><br>
	<p>Most frequent problems:
<br>1.	Website is not accessible
<br>2.	Contactdatas don’t get exported to the database
<br>3.	Databaseserver is down
<br>4.	Access denied (insufficient permissions)
<br>5.	Cronjobs don’t work
<br>6.	Scripts don’t work
<br>7.	Raspberry got no connection
<br>8.	Wrong user logged in
<br>9.	Wrong Credentials
<br>10.	SMTP Server is not accessible
<br>11.	SMTP Server changed the port
<br>12.	Too many emails at once
</p>
<br><br>1.	Website is not accessible
If the website is not accessible you should check your internet connection.
If you don’t have a connection, you should contact your provider (1&1, Vodafne, Unitymedia etc.).
<br><br>2.	Contactdatas don’t get exported to the database
The datas get transferred to the webserver. If this is not possible, the export won’t work. Please contact your system administrator.
<br><br>3.	Databaseserver is down
If the Databaseserver is not responding, please contact your system administrator.
<br><br>4.	Access denied (insufficient premissions)
If you’re missing permissions, you should check if you are logged in with your correct user. Just log out by clicking on the lock symbol in the top right section of your desktop. If the relog doesn’t work for you, please contact your system administrator.
<br><br>5.	Cronjobs don’t work
If the cronjob doesn’t run correctly, please contact your system administrator.
<br><br>6.	Scripts don’t work
In case a script doesn’t work, please contact your system administrator.
<br><br>7.	Raspberry got no connection
If the raspberry got no connection, you should try to restart it. If this doesn’t work, please contact your system administrator.
<br><br>8.	Wrong user logged in
Your client is probably logged in with a wrong account. Just relog by clicking on the lock symbol in the top right section of your desktop. If the relog doesn`t work for you, please contact your system administrator.
<br><br>9.	Wrong Credentials
If your credentials don’t work, check if you used your correct datas. If you still can’t log in, please contact your system administrator.
<br><br>10.	SMTP-Server is not accessible
If the SMTP-Server doesn’t respond, please contact your system administrator.
<br><br>11.	SMTP-Server changed the port
If the SMTP-Server changed its port, please contact your system administrator.
<br><br>12.	Too many emails at once
If you’re getting too many emails at once, the mail provider will block its services for a short period of time. Just wait a moment and try it again. If the problem lasts longer, please contact your system administrator.




	<a href="imprint.php" style="color: black">Imprint</a>    <a href="dataprotection.php" style="color: black">Privacy and Data Protection</a>    <a href="help.php" style="color: black">Help</a>
</body>
</html>	