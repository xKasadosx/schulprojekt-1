<html>
<head>
	<meta charset="UTF-8">
    <meta name="description" content="Website: PowerBad GmbH" />
    <meta name="author" content="Hertz System AG" />
	<a href="index.php"><img src="../images/powerbad_logo.bmp" border="0"></a>
</head>
<body text="#000000" bgcolor="#F0DEFF" link="#000080" alink="#FF0000" vlink="#FF0000">
	<p align="left">
	<a href="../de/impressum.php"><img src="../images/german_flag.jpg" height="25" width="37,5"/></a>
	<a href="imprint.php"><img src="../images/english_flag.jpg" height="25" width="37,5"/></a>
	</p>
	<hr/>
	<h1 align="left">Imprint</h1>
	<br>
	<br>Informations according to § 5 TMG
	<br>
	<br>PowerBad GmbH
	<br>Redinghovenstraße 16
	<br>40221 Düsseldorf
	<br>
	<br>
	<b>Contact</b>
	<br>Phone: 09920483429
	<br>Fax: 09920483410
	<br>Mail: info@powerbad.com
	<br>
	<br><b>Represented by:</b>
	<br>Director Heinz Buderus & Peter Vaillant
	<br>
	<br><b>Register entry:</b>
	<br>Registered in the commercial register.
	<br>Registercourt: Local court of Düsseldorf
	<br>Registernumber: HR B 5435000001
	<br>
	<br>Tax-ID:
	<br>Tax-Identificationsnumber to §27a Taxlaw:
	<br>DE 9231953221
	<br>
	<br>
	<hr/>
	<a href="imprint.php" style="color: black">Imprint</a>    <a href="dataprotection.php" style="color: black">Privacy and Data Protection</a>    <a href="help.php" style="color: black">Help</a>
</body>
</html>	