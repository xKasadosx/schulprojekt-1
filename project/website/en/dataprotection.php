<html>
<head>
	<meta charset="UTF-8">
    <meta name="description" content="Website: PowerBad GmbH" />
    <meta name="author" content="Hertz System AG" />
	<a href="index.php"><img src="../images/powerbad_logo.bmp" border="0"></a>
</head>
<body text="#000000" bgcolor="#F0DEFF" link="#000080" alink="#FF0000" vlink="#FF0000">
	<p align="left">
	<a href="../de/datenschutz.php"><img src="../images/german_flag.jpg" height="25" width="37,5"/></a>
	<a href="dataprotection.php"><img src="../images/english_flag.jpg" height="25" width="37,5"/></a>
	</p>
	<hr/>
	<h1>Privacy statement</h1>
	<br>
	<br><b>Privacy policy</b>
	<br>Below we would like to inform you about our privacy policy. Here you will find information about the collection and use of personal data when using our website. In doing so, we observe the data protection law applicable in Germany. You can access this declaration at any time on our website.
	<br>
	<br>We expressly point out that data transmission on the Internet (e.g. communication by e-mail) is subject to security gaps and cannot be completely protected against access by third parties.
	<br>
	<br>The use of the contact data of our imprint for commercial advertising is expressly not desired, unless we have previously given our written consent or there is already a business relationship. The provider and all persons named on this website hereby object to any commercial use and disclosure of their data.
	<br>
	<br>
	<br><b>Personal data</b>
	<br>You can visit our website without providing any personal data. Insofar as personal data (such as name, address or e-mail address) is collected on our pages, this is done on a voluntary basis as far as possible. This data will not be passed on to third parties without your express consent. If a contractual relationship is to be established between you and us, or if its content is to be modified or amended, or if you send us an enquiry, we will collect and use personal data from you to the extent necessary for these purposes (inventory data). We collect, process and use personal data to the extent necessary to enable you to use the website (usage data). All personal data will only be stored as long as it is necessary for the stated purpose (processing your request or processing a contract). Tax and commercial retention periods are taken into account. By order of the responsible authorities, we may in individual cases provide information about this data (inventory data), insofar as this is necessary for the purposes of criminal prosecution, for averting danger, for fulfilling the statutory duties of the constitutional protection authorities or the Military Counter-Intelligence Service or for enforcing intellectual property rights.
	<br>
	<br>
	<hr/>
	<a href="imprint.php" style="color: black">Imprint</a>    <a href="dataprotection.php" style="color: black">Privacy and Data Protection</a>    <a href="help.php" style="color: black">Help</a>
</body>
</html>	