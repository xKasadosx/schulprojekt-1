<?php
	include_once '../database/clientDBConnect.php';
?>	

<html>
<head>
	<meta charset="UTF-8">
    <meta name="description" content="Website: PowerBad GmbH" />
    <meta name="author" content="Hertz System AG" />
	<a href="../index.php"><img src="../images/powerbad_logo.bmp" border="0"></a>
</head>

<body text="#000000" bgcolor="#F0DEFF" link="#000080" alink="#FF0000" vlink="#FF0000">
	<p align="left">
	<a href="../de/kontaktformular.php"><img src="../images/german_flag.jpg" height="25" width="37,5"/></a>
	<a href="contactform.php"><img src="../images/english_flag.jpg" height="25" width="37,5"/></a>
	</p>
	<hr/>
	<h1>Contact form</h1>
	<br>
	<br>Please fill in the fields.
	<br>
	<br>
	<form method="POST">
		<table>
			<tr>
				<td>Your request</td>
				<td>
					<select name="optionSubject">
						<option value="">--Selection--</option>
						<option value="Angebot">Supply</option>
						<option value="Reklamation">Complaint</option>
						<option value="Beschwerde">Grievance</option>
						<option value="Infomaterial">Informations</option>
						<option value="Diverse">Diverses</option>
					 </select>
				</td>
			</tr>
			<tr>
				<td>Title</td>
				<td>
					<select name="optionClientFormOfAdress">
						<option value="">--Selection--</option>
						<option value="Herr">Mr.</option>
						<option value="Frau">Mrs.</option>
						<option value="Firma">Company</option>
					 </select>
				</td>
			</tr>
			<tr>
				<td>First name</td>
				<td>
				 <input type="text" name="textClientForname" value=""><br>
				</td>	
			</tr>
			<tr>
				<td>Surname</td>
				<td>
				 <input type="text" name="textClientSurname" value=""><br>
				</td>
			</tr>
			<tr>
				<td>Company</td>
				<td>
				 <input type="text" name="textCompanyName" value=""><br>
				</td>	
			</tr>			
			<tr>
				<td>Streetname</td>
				<td>
				 <input type="text" name="textStreetName" value=""><br>
				</td>	
			</tr>			
			<tr>
				<td>Housenumber</td>
				<td>
				 <input type="text" name="textHouseNumber" value=""><br>
				</td>	
			</tr>						
			<tr>
				<td>City</td>
				<td>
				 <input type="text" name="textPlaceName" value=""><br>
				</td>	
			</tr>	
			<tr>
				<td>Postcode</td>
				<td>
				 <input type="number" name="numberPostCode" value=""><br>
				</td>	
			</tr>			
			<tr>
				<td>Email</td>
				<td>
				 <input type="text" name="textClientEmailAdress" value=""><br>
				</td>	
			</tr>
			<tr>
				<td>Phonenumber</td>
				<td>
				 <input type="number" name="numberClientPhoneNumber" value=""><br>
				</td>	
			</tr>
			<tr>
				<td>Optional Information</td>
				<td>
				<textarea name="textOptionalInformation" rows="3" cols="25"></textarea>
				</td>	
			</tr>
		</table>
		I have read the <a href="dataprotection.php" style="color: red" target="_blank">privacy statement</a> and I agree with this.
		<input type="checkbox" name="checkDataProtection" value="no"/>
		<p>
		<input type="submit" name="buttonSubmit" value="senden"/>
	</form>
	</body>
</html>

<?php
if (isset($_POST["buttonSubmit"])){
	if (mysqli_connect_errno()) {
		printf("Connect failed: %s\n", mysqli_connect_error());
		exit();
	}
	
	$clientLanguage = "Deutsch";
	$clientSubject = $_POST["optionSubject"];
	$clientFormOfAdress = $_POST["optionClientFormOfAdress"];
	$clientForname = $_POST["textClientForname"];
	$clientSurname = $_POST["textClientSurname"];
	$clientCompanyName = $_POST["textCompanyName"];
	$clientStreetName = $_POST["textStreetName"];
	$clientHouseNumber = $_POST["textHouseNumber"];
	$clientPlaceName = $_POST["textPlaceName"]; //move
	$clientNumberPostCode = $_POST["numberPostCode"];
	$clientPhoneNumber = $_POST["numberClientPhoneNumber"];
	$clientEmailAdress = $_POST['textClientEmailAdress'];
	$clientOptionalInformation = $_POST["textOptionalInformation"];
	$currentTimestamp = date('Y-m-d H:m:s');
	$dataprotection = isset($_POST['checkDataProtection']);

	$sqlCommand = "INSERT INTO contactdata VALUES('$clientLanguage','$clientSubject','$clientFormOfAdress', '$clientForname', '$clientSurname', '$clientCompanyName', '$clientStreetName', '$clientHouseNumber','$clientPlaceName','$clientNumberPostCode', '$clientPhoneNumber','$clientEmailAdress','$clientOptionalInformation','$currentTimestamp','$dataprotection')";

	if($clientSubject === "" || $clientFormOfAdress === "" || $clientForname === "" || $clientSurname === "" ||
	$clientStreetName === "" || $clientHouseNumber === "" ||$clientNumberPostCode === "" || $clientPlaceName === "" ||
	$clientPhoneNumber === "" || $clientEmailAdress === "" ){
		echo "<font color='#ff0000'>Please fill out all fields.</font>";
	}
	else{
		if($dataprotection){	
			if ($database->query($sqlCommand) === TRUE) {
				echo "Your data got recorded.";
			} 
			else{
				echo "Error: " . $sqlCommand . "<br>" . $database->error;
			}	
		}	
		else{
			echo "<font color='#ff0000'>Please accept the privacy statement</font>";
		}
	}
	$database->close();	
}
?>
	<br>
	<br>
	<hr />
	<a href="imprint.php" style="color: black">Imprint</a>    <a href="dataprotection.php" style="color: black">Privacy and Data Protection</a>    <a href="help.php" style="color: black">Help</a>
</body>
</html>	