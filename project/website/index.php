﻿<html>
<head>
	<meta charset="UTF-8">
    <meta name="description" content="Website: PowerBad GmbH" />
    <meta name="author" content="Hertz System AG" />
	<a href="index.php"><img src="images/powerbad_logo.bmp" border="0"></a>
</head>

<body text="#000000" bgcolor="#F0DEFF" link="#000080" alink="#FF0000" vlink="#FF0000">
	<p align="left">
	<a href="index.php"><img src="images/german_flag.jpg" height="25" width="37,5"/></a>
	<a href="en/index.php"><img src="images/english_flag.jpg" height="25" width="37,5"/></a>
	</p>
	<hr/>
	<h2>Wer ist die PowerBad GmbH?</h2>
	<p>Die PowerBad GmbH ist ein mittelständisches Unternehmen. 
	<br/>Unsere Mitarbeiter und wir stehen für ein solides Handwerk und zuverlässige Auftragsbearbeitung mit Kompetenz, Engagement und langjähriger Berufserfahrung. 
	</p>
	<h2>Welchen Aufgabenbereich haben wir? </h2>
	<p>Unsere Arbeitsgebiete umfassen die innovative und moderne Sanitärtechnik (vom Hausbau bis zu senioren- bzw. behindertengerechten Lösungen),
	<br>hocheffiziente und zeitgemäße Kälte- (Klimaanlagen) und Wärmetechnik (Brennwerttechnik, Feststoffkessel, Ölheizung, Wärmepumpen oder die Optimierung Ihrer Zentralheizung) sowie Solartechnologien.
	</p>
	<p>
	<h2>Was können wir für Sie tun?</h2>
	Der Arbeitsschwerpunkt des Unternehmens mit aktuell 14 Mitarbeitern liegt bei der Projektierung von kompletten Renovierungen und der Erstellung, Wartung, Erweiterung und Optimierung größerer Heizungsanlagen, mit Solar- oder Wärmepumpentechnik.
	</p>
	<p>
	<h2>Haben Wir ihr Interesse geweckt?</h2>
	<br/>Sie möchten weitere Informationen zu uns haben? Eventuell sogar ein Angebot?
	<br/> Hinterlassen Sie uns Ihre Daten und ihr Anliegen in unserem <a href="de/kontaktformular.php" style="color: red">Kontaktformular</a>, wir werden auf sie zukommen!
	</p>

	<hr/>
	<a href="de/impressum.php" style="color: black">Impressum</a>    <a href="de/datenschutz.php" style="color: black">Datenschutz</a>    <a href="de/hilfe.php" style="color: black">Hilfe</a> <a href="admin/login.php" style="color: black">Login</a>
</body>
</html>	