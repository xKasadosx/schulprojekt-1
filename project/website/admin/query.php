<?php
	include_once '../database/adminDBConnect.php';
?>	
<html>
<head>
	<meta charset="UTF-8">
    <meta name="description" content="Website: PowerBad GmbH" />
    <meta name="author" content="Hertz System AG" />
	<a href="../index.php"><img src="../images/powerbad_logo.bmp" border="0"></a>
</head>

<body text="#000000" bgcolor="#F0DEFF" link="#000080" alink="#FF0000" vlink="#FF0000">
	<p align="left">
	</p>
	<hr/>
	<h1>Datenbankabfrage</h1>
	<form action="../index.php" method="POST">
		<input type="submit" name="buttonSubmit" value="Logout"/>
	</form>
	<br>
	<br>
	<br>
	<form action="query.php" method="POST"> 
	<table>
		<tr>
			<td>Welche Daten wollen sie sehen?</td>
		</tr>
		<tr>
			<td>
				<select name="optionTable">
					<option value="">--Auswahl--</option>
					<option value="client">Kundendaten</option>
					<option value="raspberry">Raspberry Temp.</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Wie alt sollen die Daten sein?</td>
		</tr>
		<tr>
			<td>
				<select name="optionDate">
					<option value="oneHour">1. Stunde</option>
					<option value="sixHours">6. Stunden</option>
					<option value="twelveHours">12. Stunden</option>
					<option value="twentyfourHours">24. Stunden</option>
					<option value="older">Alle</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Wie viele aufgaben wollen sie Sehen?</td>
		</tr>
		<tr>
			<td>
				<select name="optionLimit">
					<option value="five">5</option>
					<option value="ten">10</option>
					<option value="fifteen">15</option>
					<option value="thirty">30</option>
					<option value="all">Alle</option>
				</select>
			</td>
		</tr>
		<tr>
			<td>Sortierung:</td>
		</tr>
		<tr>
			<td>
			aufsteigend <input type="radio" name="optionSort" checked value="desc"> 
			absteigend <input type="radio" name="optionSort" value="asc"> 
			</td>
		</tr>
	</table>
		<input type="submit" name="buttonSubmit" value="senden"/>
	</form> 	

<?php
if (isset($_POST["buttonSubmit"])){
	if (mysqli_connect_errno()) {
		printf("Connect failed: %s\n", mysqli_connect_error());
		exit();
	}

	

	$optionTable = $_POST["optionTable"];
	$optionDate = $_POST["optionDate"];
	$optionLimit = $_POST["optionLimit"];
	$optionSort = $_POST["optionSort"];

	if($optionTable === "client"){
		$sqlTable = "select * from contactdata ";
	}
	else if ($optionTable === "raspberry"){
		$sqlTable = "select * from temperature ";
	}
	else{
		$sqlTable = "";
		echo "Tabelle aussuchen!";
	}

	switch ($optionDate) {
		case "oneHour":
			$timestamp = date_create(date('Y-m-d H:m:s'))->modify('-1 hours')->format('Y-m-d H:m:s');
			$sqlWhere = "where Timestamp >= '$timestamp' ";
			break;
		case "sixHours":
			$timestamp = date_create(date('Y-m-d H:m:s'))->modify('-6 hours')->format('Y-m-d H:m:s');
			$sqlWhere = "where Timestamp >= '$timestamp' ";
			break;
		case "twelveHours":
			$timestamp = date_create(date('Y-m-d H:m:s'))->modify('-12 hours')->format('Y-m-d H:m:s');
			$sqlWhere = "where Timestamp >= '$timestamp' ";
			break;
		case "twentyfourHours":
			$timestamp = date_create(date('Y-m-d H:m:s'))->modify('-24 hours')->format('Y-m-d H:m:s');
			$sqlWhere = "where Timestamp >= '$timestamp' ";
			break;
		case "older":
			$sqlWhere = "";
			break;
	}
	switch ($optionLimit) {
		case "five":
			$sqlLimit = "limit 5";
			break;
		case "ten":
			$sqlLimit = "limit 10";
			break;
		case "fifteen":
			$sqlLimit = "limit 15";
			break;
		case "thirty":
			$sqlLimit = "limit 30";
			break;
		case "all":
			$sqlWhere = "";
			break;
	}

	if($optionSort === "asc"){$sqlSort = "asc ";}
	else{$sqlSort = "desc";}
	
	$sqlCommand = "$sqlTable $sqlWhere order by Timestamp $sqlSort $sqlLimit";

	if($database->multi_query($sqlCommand)){
		do{
			$result = $database->store_result();
			
			echo "</br>Insgesamte Datenreihen: ".$result->num_rows;
			echo "<hr width='190px' align='left'/>";
			$datatuple = $result->fetch_fields();
			echo "<table border='1'>";
			echo "<tr>";
			foreach($datatuple as $data){
				echo "<th>".$data->name."</th>";
			}
			echo "</tr>";
			
			while($row = $result->fetch_assoc()){
				echo "<tr>";
				foreach($row as $datarow){
					echo "<td>".$datarow."</td>";
				}
				echo "</tr>";
			}	
			echo "</table>";
		}while($database->more_results() && $database->next_result());
	}

}

	
	$database->close();	
?>
	<br>
	<br>
	<hr/>
</body>
</html>	