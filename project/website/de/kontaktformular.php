<?php
	include_once '../database/clientDBConnect.php';
?>	

<html>
<head>
	<meta charset="UTF-8">
    <meta name="description" content="Website: PowerBad GmbH" />
    <meta name="author" content="Hertz System AG" />
	<a href="../index.php"><img src="../images/powerbad_logo.bmp" border="0"></a>
</head>

<body text="#000000" bgcolor="#F0DEFF" link="#000080" alink="#FF0000" vlink="#FF0000">
	<p align="left">
	<a href="kontaktformular.php"><img src="../images/german_flag.jpg" height="25" width="37,5"/></a>
	<a href="../en/contactform.php"><img src="../images/english_flag.jpg" height="25" width="37,5"/></a>
	</p>
	<hr/>
	<h1>Kontaktformular</h1>
	<br>
	<br>Bitte füllen sie alle Felder aus.
	<br>
	<br>
	<form method="POST">
		<table>
			<tr>
				<td>Ihr Anliegen</td>
				<td>
					<select name="optionSubject">
						<option value="">--Auswahl--</option>
						<option value="Angebot">Angebot</option>
						<option value="Reklamation">Rekalamtion</option>
						<option value="Beschwerde">Beschwerde</option>
						<option value="Infomaterial">Infomaterial</option>
						<option value="Diverse">Diverses</option>
					 </select>
				</td>
			</tr>
			<tr>
				<td>Anrede</td>
				<td>
					<select name="optionClientFormOfAdress">
						<option value="">--Auswahl--</option>
						<option value="Herr">Herr</option>
						<option value="Frau">Frau</option>
						<option value="Firma">Firma</option>
					 </select>
				</td>
			</tr>
			<tr>
				<td>Vorname</td>
				<td>
				 <input type="text" name="textClientForname" value=""><br>
				</td>	
			</tr>
			<tr>
				<td>Nachname</td>
				<td>
				 <input type="text" name="textClientSurname" value=""><br>
				</td>
			</tr>
			<tr>
				<td>Firma</td>
				<td>
				 <input type="text" name="textCompanyName" value=""><br>
				</td>	
			</tr>			
			<tr>
				<td>Straße</td>
				<td>
				 <input type="text" name="textStreetName" value=""><br>
				</td>	
			</tr>			
			<tr>
				<td>Hausnummer</td>
				<td>
				 <input type="text" name="textHouseNumber" value=""><br>
				</td>	
			</tr>						
			<tr>
				<td>Ort</td>
				<td>
				 <input type="text" name="textPlaceName" value=""><br>
				</td>	
			</tr>	
			<tr>
				<td>Postleitzahl</td>
				<td>
				 <input type="number" name="numberPostCode" value=""><br>
				</td>	
			</tr>			
			<tr>
				<td>Email</td>
				<td>
				 <input type="text" name="textClientEmailAdress" value=""><br>
				</td>	
			</tr>
			<tr>
				<td>Telefonnummer</td>
				<td>
				 <input type="number" name="numberClientPhoneNumber" value=""><br>
				</td>	
			</tr>
			<tr>
				<td>Zusatzinformation</td>
				<td>
				<textarea name="textOptionalInformation" rows="3" cols="25"></textarea>
				</td>	
			</tr>
		</table>
		Ich habe die geltende <a href="datenschutz.php" style="color: red" target="_blank">Datenschutzerklärung</a> gelesen und stimme zu.
		<input type="checkbox" name="checkDataProtection" value="no"/>
		<p>
		<input type="submit" name="buttonSubmit" value="senden"/>
	</form>
	</body>
</html>

<?php
if (isset($_POST["buttonSubmit"])){
	if (mysqli_connect_errno()) {
		printf("Connect failed: %s\n", mysqli_connect_error());
		exit();
	}
	
	$clientLanguage = "Deutsch";
	$clientSubject = $_POST["optionSubject"];
	$clientFormOfAdress = $_POST["optionClientFormOfAdress"];
	$clientForname = $_POST["textClientForname"];
	$clientSurname = $_POST["textClientSurname"];
	$clientCompanyName = $_POST["textCompanyName"];
	$clientStreetName = $_POST["textStreetName"];
	$clientHouseNumber = $_POST["textHouseNumber"];
	$clientPlaceName = $_POST["textPlaceName"]; //move
	$clientNumberPostCode = $_POST["numberPostCode"];
	$clientPhoneNumber = $_POST["numberClientPhoneNumber"];
	$clientEmailAdress = $_POST['textClientEmailAdress'];
	$clientOptionalInformation = $_POST["textOptionalInformation"];
	$currentTimestamp = date('Y-m-d H:m:s');
	$dataprotection = isset($_POST['checkDataProtection']);

	$sqlCommand = "INSERT INTO contactdata VALUES('$clientLanguage','$clientSubject','$clientFormOfAdress', '$clientForname', '$clientSurname', '$clientCompanyName', '$clientStreetName', '$clientHouseNumber','$clientPlaceName','$clientNumberPostCode', '$clientPhoneNumber','$clientEmailAdress','$clientOptionalInformation','$currentTimestamp','$dataprotection')";

	if($clientSubject === "" || $clientFormOfAdress === "" || $clientForname === "" || $clientSurname === "" ||
	$clientStreetName === "" || $clientHouseNumber === "" ||$clientNumberPostCode === "" || $clientPlaceName === "" ||
	$clientPhoneNumber === "" || $clientEmailAdress === "" ){
		echo "<font color='#ff0000'>Bitte füllen sie ALLE Felder aus.</font>";
	}
	else{
		if($dataprotection){	
			if ($database->query($sqlCommand) === TRUE) {
				echo "Ihre Daten wurden erfolgreich gespeichert.";
			} 
			else{
				echo "Error: " . $sqlCommand . "<br>" . $database->error;
			}	
		}	
		else{
			echo "<font color='#ff0000'>Bestätigen sie bitte die Datenschutzrichtlinien</font>";
		}
	}
	$database->close();	
}
?>
	<br>
	<br>
	<hr />
	<a href="impressum.php" style="color: black">Impressum</a>    <a href="datenschutz.php" style="color: black">Datenschutz</a>    <a href="hilfe.php" style="color: black">Hilfe</a>
</body>
</html>	