<html>
<head>
	<meta charset="UTF-8">
    <meta name="description" content="Website: PowerBad GmbH" />
    <meta name="author" content="Hertz System AG" />
	<a href="../index.php"><img src="../images/powerbad_logo.bmp" border="0"></a>
</head>

<body text="#000000" bgcolor="#F0DEFF" link="#000080" alink="#FF0000" vlink="#FF0000">
	<p align="left">
	<a href="hilfe.php"><img src="../images/german_flag.jpg" height="25" width="37,5"/></a>
	<a href="../en/help.php"><img src="../images/english_flag.jpg" height="25" width="37,5"/></a>
	</p>
	<hr/>
	<h1>Hilfe</h1>
	<hr/>
<p><br>Häufigsten Probleme:
<br>1.	Webseite ist nicht aufrufbar
<br>2.	Kontaktdaten werden nicht in die Datenbank exportiert
<br>3.	Datenbank ist down
<br>4.	Zugriff verweigert (Fehlende Berechtigung)
<br>5.	Cronjob wird nicht ausgeführt
<br>6.	Script funktioniert nicht mehr
<br>7.	Raspberry hat keine Verbindung 
<br>8.	Falscher User ist eingeloggt 
<br>9.	Falsche Credentials
<br>10.	SMTP Server ist nicht erreichbar
<br>11.	SMTP Server hat den Port gewechselt
<br>12.	Zu viele E-Mails auf einmal -> Block
</p>	
<br>1.	Webseite ist nicht aufrufbar
Sollte die Webseite nicht aufrufbar sein sollten Sie die Internetverbindung überprüfen.
Wenn die Verbindung nicht besteht sollten Sie Kontakt zu Ihrem Provider (1&1, Vodafone, Unitymedia etc.) aufnehmen.
<br><br>2.	Kontaktdaten werden nicht in die Datenbank exportiert
Der Export der Daten in die Datenbank erfolgt über den Webserver. Sollte dieser nicht erreichbar sein, ist ein Export nicht möglich. Bitte wenden Sie sich an den Systemadministrator.
<br><br>3.	Datenbank ist nicht erreichbar
Sollte der Datenbankserver nicht erreichbar sein wenden Sie sich bitte an den Systemadministrator.
<br><br>4.	Fehlende Berechtigungen
Sollten Ihnen Berechtigungen fehlen, besteht die Möglichkeit das Sie sich mit einem falschen Benutzer angemeldet haben oder der vorherige Benutzer noch angemeldet ist. Sollte dies nicht mit einer Anmeldung mit Ihrem richtigen User behoben sein, wenden Sie sich an den Systemadministrator.
<br><br>5.	Cronjob wird nicht ausgeführt
Wenn der Cronjob nicht ausgeführt wird wenden Sie sich bitte an den Systemadministrator.
<br><br>6.	Script funktioniert nicht mehr
Wenn ein Script, was für einen Prozess benötigt wird, nicht mehr funktioniert, wenden Sie sich bitte an den Systemadministrator. 
<br><br>7.	Raspberry hat keine Verbindung 
Sollte der Raspberry keine Verbindung herstellen können, versuchen Sie zuerst diesen neu zu Starten.
Wenn das nicht hilft, wenden Sie sich an den Systemadministrator.
<br><br>8.	Falscher User ist eingeloggt 
Sollten Sie mit dem Falschen User eingeloggt sein, melden Sie sich ab. Zum abmelden Klicken Sie auf den Pfeil oben rechts und auf Abmelden. Dann melden Sie sich mit Ihren Daten an.
<br><br>9.	Falsche Credentials
Sollten Fehler in den Credentials auftreten, wenden Sie sich an den Systemadministrator.
<br><br>10.	SMTP Server ist nicht erreichbar
Wenn der SMTP Server nicht erreichbar ist, wenden Sie sich bitte an den Systemadministrator.
<br><br>11.	SMTP Server hat den Port gewechselt
Wenn der SMTP Server den Port gewechselt hat, wenden Sie sich an den Systemadministrator.
<br><br>12.	Zu viele E-Mails auf einmal -> Block
Sollten zu viele E-Mails auf einmal eingehen, wird der Mail-Provider  für eine kurze Zeit den Dienst blockieren. Warten Sie einige Minuten und versuchen Sie dann erneut nach den E-Mails zu sehen. Sollten sich die Probleme so nicht lösen lassen, wenden Sie sich an Ihren Systemadministrator.



	<hr />
	<a href="impressum.php" style="color: black">Impressum</a>    <a href="datenschutz.php" style="color: black">Datenschutz</a>    <a href="hilfe.php" style="color: black">Hilfe</a>
</body>
</html>	