<html>
<head>
	<meta charset="UTF-8">
    <meta name="description" content="Website: PowerBad GmbH" />
    <meta name="author" content="Hertz System AG" />
	<a href="../index.php"><img src="../images/powerbad_logo.bmp" border="0"></a>
</head>
<body text="#000000" bgcolor="#F0DEFF" link="#000080" alink="#FF0000" vlink="#FF0000">
	<p align="left">
	<a href="impressum.php"><img src="../images/german_flag.jpg" height="25" width="37,5"/></a>
	<a href="../en/imprint.php"><img src="../images/english_flag.jpg" height="25" width="37,5"/></a>
	</p>
	<hr/>
	<h1>Impressum</h1>
	<br>Angaben gemäß § 5 TMG
	<br>
	<br>PowerBad GmbH
	<br>Redinghovenstraße 16
	<br>40221 Düsseldorf
	<br>
	<br>
	<b>Kontakt</b>
	<br>Telefon: 09920483429
	<br>Telefax: 09920483410
	<br>E-Mail: info@powerbad.com
	<br>
	<br><b>Vertreten durch:</b>
	<br>Geschäftsführer Heinz Buderus & Peter Vaillant
	<br>
	<br><b>Registereintrag:</b>
	<br>Eingetragen im Handelsregister.
	<br>Registergericht: Amtsgericht Düsseldorf
	<br>Registernummer: HR B 5435000001
	<br>
	<br>Umsatzsteuer-ID:
	<br>Umsatzsteuer-Identifikationsnummer nach §27a Umsatzsteuergesetz:
	<br>DE 9231953221
	<br>
	<br>
	<hr />
	<a href="impressum.php" style="color: black">Impressum</a>    <a href="datenschutz.php" style="color: black">Datenschutz</a>    <a href="hilfe.php" style="color: black">Hilfe</a>
</body>
</html>	