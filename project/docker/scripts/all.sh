#!/bin/bash
docker build -f ../httpd/dockerfile-latest -t web:latest .

docker run -d --name web -v /home/user/website/:/var/www/html -p 80:80 --restart always web:latest
firewall-cmd --zone=public --add-service=http
firewall-cmd --zone=public --permanent --add-service=http

docker run --env LDAP_ADMIN_PASSWORD="123456" --env LDAP_ORGANISATION="powerbad" --env LDAP_DOMAIN="powerbad.net" -p 389:389 -p 636:636 --name ldap --hostname ldap.powerbad.net --volume /home/user/ldap/database:/var/lib/ldap --detach --restart always osixia/openldap:1.2.4
firewall-cmd --zone=public --add-port=389/tcp
firewall-cmd --zone=public --add-port=389/tcp --permanent
firewall-cmd --zone=public --add-port=636/tcp
firewall-cmd --zone=public --add-port=636/tcp --permanent
firewall-cmd --add-service=ldap
firewall-cmd --add-service=ldap --permanent

docker run -d -p 3306:3306 --name=mysql-server -v /home/user/mysql/:/var/lib/mysql --env="MYSQL_ROOT_PASSWORD=123456" --restart always mysql
firewall-cmd --zone=public --add-port=3306/tcp
firewall-cmd --zone=public --permanent --add-port=3306/tcp

docker run --name myadmin -d -e PMA_HOST=192.168.100.200 --link mysql-server:mysql-server -p 8080:80 --restart always phpmyadmin/phpmyadmin
