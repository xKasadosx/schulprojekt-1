#!/bin/bash
docker build -f ../httpd/dockerfile-latest -t web:latest .

docker run -d --name web -v /home/user/website/:/var/www/html -p 80:80 --restart always web:latest
firewall-cmd --zone=public --add-service=http
firewall-cmd --zone=public --permanent --add-service=http
