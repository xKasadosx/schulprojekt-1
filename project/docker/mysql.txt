docker run -d -p 3306:3306 --name=mysql-server -v /home/user/mysql/:/var/lib/mysql --env="MYSQL_ROOT_PASSWORD=123456" --restart always mysql

#docker comit mysql-server mysql-server:mysql-server
#docker run -d -p 3306:3306 --name=mysql-server -v /home/user/mysql/:/var/lib/mysql --env="MYSQL_ROOT_PASSWORD=123456" --restart always mysql:mysql

sudo firewall-cmd --zone=public --add-port=3306/tcp
sudo firewall-cmd --zone=public --permanent --add-port=3306/tcp

sudo docker exec -it -u 0 mysql-server bash
apt-get update
apt-get install vim -y
vim /etc/mysql/my.cnf
default-authentication-plugin=mysql_native_password


CREATE USER 'powerbad'@'172.17.0.1' IDENTIFIED BY '!Pgfs13';

Grant all on powerbad_client_db.* to 'powerbad'@'172.17.0.1';

CREATE USER 'admin'@'172.17.0.1' IDENTIFIED BY '!Pgfs13';

Grant all on powerbad_client_db.* to 'admin'@'172.17.0.1';
