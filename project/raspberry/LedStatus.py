#!/usr/bin/env python3

#IMPORT LIBS
import RPi.GPIO as GPIO

def inital_leds():
	GPIO.setmode(GPIO.BOARD)
	GPIO.setwarnings(False)
	global gpio
	
	#40=green 38=yellow 36=red
	gpio = [29,31,33]
	GPIO.setup(gpio[0], GPIO.OUT, initial=GPIO.LOW) 
	GPIO.setup(gpio[1], GPIO.OUT, initial=GPIO.LOW)
	GPIO.setup(gpio[2], GPIO.OUT, initial=GPIO.LOW)

def clear_led_status():
	GPIO.cleanup()

def led_red():
	GPIO.output(gpio[0], GPIO.LOW)
	GPIO.output(gpio[1], GPIO.LOW)	
	GPIO.output(gpio[2], GPIO.HIGH)

def led_yellow():
	GPIO.output(gpio[0], GPIO.LOW)
	GPIO.output(gpio[1], GPIO.HIGH)	
	GPIO.output(gpio[2], GPIO.HIGH) 
	
def led_green():
	GPIO.output(gpio[0], GPIO.LOW)
	GPIO.output(gpio[1], GPIO.HIGH)	
	GPIO.output(gpio[2], GPIO.LOW)
	
