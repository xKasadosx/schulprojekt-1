#!/usr/bin/env python3

#credential file
import Credentials as credential

#IMPORT LIBS
import subprocess
import smtplib
import socket
import datetime
import sys
from email.mime.text import MIMEText

def send_email_to_office(CLIENTNAME,boilertemperature,status,timestamp):
	receiver = credential.smtpReceiver
	sender = credential.smtpSender
	password = credential.smtpPassword
	smtpserver = smtplib.SMTP(credential.smtpAdress, credential.smtpPort)
	smtpserver.ehlo()
	smtpserver.starttls()
	smtpserver.ehlo
	smtpserver.login(sender, password)

	Wert = "Status: %s \n Zeit: %s \nKunde: %s \nBoilertemperatur: %s C \n\n\nSeit %s wird bei Kunde: %s der Status: %s uebermittelt." %(status,timestamp,CLIENTNAME,boilertemperature,timestamp,CLIENTNAME,status)
	
	for Argument in range(1, len(sys.argv)):
		Wert += str(sys.argv[Argument])
		Wert += " "

	msg = MIMEText(Wert)
	msg['Subject'] = 'Alarm: %s - %s -%s' %(status,CLIENTNAME,timestamp)
	msg['From'] = sender
	msg['To'] = receiver

	smtpserver.sendmail(sender, [receiver], msg.as_string())
	smtpserver.quit()
	()
