#!/usr/bin/env python3

#credential file
import Credentials as credential

#SQL IMPORTS
import MySQLdb

#CONNECT TO DATABASE
def connect_to_database():
	global dbconnect
	dbconnect = MySQLdb.connect(
		host = credential.databaseHost,
		user = credential.databaseUser,
		passwd = credential.databasePassword,
		db = credential.database
	)

def send_data_to_database(status, CLIENTNAME,roomtemperature,boilertemperature,timestamp):	
	connect_to_database()
	
	sqlInsert = """INSERT INTO temperature VALUES(%s, %s, %s, %s, %s)"""

	insert = dbconnect.cursor()
	insert.execute(sqlInsert, (status, CLIENTNAME, roomtemperature, boilertemperature, timestamp))
	dbconnect.commit()
	dbconnect.close
