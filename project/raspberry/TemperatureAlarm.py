#!/usr/bin/env python3

#file import
import TempSensor as sensor
import LedStatus as setLedStatus
import DBfunction as database
import SMTPMail as sendSMTPEmail

#IMPORT LIBS
from datetime import datetime

#IMPORT CONSTANS
import Constants as constant

#SET VARIABLES
status = ["OK","WARNING","CRITICAL"]
timestamp = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
sendmail = False
boilertemperature = sensor.read_temp()

setLedStatus.inital_leds()

if boilertemperature >= constant.BOILERTEMPCRIT:
	status = status[2]
	sendmail = True
	setLedStatus.led_red()
		
elif boilertemperature < constant.BOILERTEMPCRIT and boilertemperature >= constant.BOILERTEMPWARN:
	status = status[1]
	setLedStatus.led_yellow()

elif boilertemperature < constant.BOILERTEMPWARN:
	status = status[0]
	setLedStatus.led_green()

database.connect_to_database()
database.send_data_to_database(status,constant.CLIENTNAME,boilertemperature,timestamp)
	
if sendmail:
		sendSMTPEmail.send_email_to_office(constant.CLIENTNAME,boilertemperature,status,timestamp)
