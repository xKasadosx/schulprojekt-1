-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 28. Mai 2019 um 11:41
-- Server-Version: 10.1.40-MariaDB
-- PHP-Version: 7.3.5

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `powerbad_client_db`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `contactdata`
--

CREATE TABLE `contactdata` (
  `Language` varchar(10) COLLATE utf8_german2_ci NOT NULL,
  `Subject` varchar(15) COLLATE utf8_german2_ci NOT NULL,
  `Formofadress` varchar(15) COLLATE utf8_german2_ci NOT NULL,
  `Forname` varchar(30) COLLATE utf8_german2_ci NOT NULL,
  `Surname` varchar(30) COLLATE utf8_german2_ci NOT NULL,
  `Company` varchar(50) COLLATE utf8_german2_ci DEFAULT NULL,
  `Streetname` varchar(50) COLLATE utf8_german2_ci NOT NULL,
  `Housenumber` text COLLATE utf8_german2_ci NOT NULL,
  `Placename` varchar(30) COLLATE utf8_german2_ci NOT NULL,
  `Postcode` text COLLATE utf8_german2_ci NOT NULL,
  `Phonenumber` text COLLATE utf8_german2_ci NOT NULL,
  `Emailadress` text COLLATE utf8_german2_ci NOT NULL,
  `Optionalinfo` text COLLATE utf8_german2_ci,
  `Timestamp` text COLLATE utf8_german2_ci NOT NULL,
  `Dataprotection` text COLLATE utf8_german2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_german2_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `temperature`
--

CREATE TABLE `temperature` (
  `status` varchar(10) COLLATE utf8_german2_ci NOT NULL,
  `clientname` varchar(30) COLLATE utf8_german2_ci NOT NULL,
  `tempboiler` float NOT NULL,
  `timestamp` text COLLATE utf8_german2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_german2_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `user`
--

CREATE TABLE `user` (
  `user` varchar(20) COLLATE utf8_german2_ci NOT NULL,
  `password` text COLLATE utf8_german2_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_german2_ci;

--
-- Daten für Tabelle `user`
--

INSERT INTO `user` (`user`, `password`) VALUES
('admin', 'admin');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
