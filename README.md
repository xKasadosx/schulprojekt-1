School Project description:
Our job was to make it as cheap as possible, to build a website,database,server,clients,temperaturesensor and everything should be connected.
Company "PowerBad GmbH" needs:
A Website with connection to a database to keep customer information
A Raspberrypi with an temperature sensor who is connected to a database and sends informationen about the temperature and status, also sends an email to the Powerbad GmbH if its critical, and a LED shows the status (green,yellow,red)
The Raspberry has an Python program that is very agile and dynamic to read the sensor,send the email, set led status, configure variables and so on.
A Database (MySQL) for the customer information and Raspberry sensor.
Docker to run the web,database and freeipa server.
Also we had to backup the Server. We used a raid 10 system with 4 hard drives and gitlab as another backup for our Docker containers with all data.